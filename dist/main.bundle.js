webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-square *ngFor=\"let i of squares\" [square]=\"i\" (userClick)=\"click($event)\"></app-square>"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__square_square_service__ = __webpack_require__("../../../../../src/app/square/square.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sequence_sequence_service__ = __webpack_require__("../../../../../src/app/sequence/sequence.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(squareService, sequenceService) {
        this.squareService = squareService;
        this.sequenceService = sequenceService;
        this.init();
    }
    AppComponent.prototype.init = function () {
        // Reset Lists
        this.squareService.reset();
        this.sequenceService.reset();
        // Generate Squares
        this.squareService.add('Blue');
        this.squareService.add('DarkOliveGreen');
        this.squareService.add('CornflowerBlue');
        this.squareService.add('HotPink');
        this.squareService.add('Chocolate');
        this.squareService.add('DarkMagenta');
        this.squareService.add('Gold');
        this.squareService.add('GreenYellow');
        // Get List of Sqaures from SquareService
        this.squares = this.squareService.getAll();
        // Get List of Available Sequence IDs
        this.available = __WEBPACK_IMPORTED_MODULE_1_lodash___default.a.map(this.squares, function (o) { return o.id; });
        // Generate Initial Sequence
        var random;
        for (var i = 0; i < 3; i++) {
            random = Math.floor(Math.random() * this.available.length);
            this.sequenceService.add(this.available[random]);
        }
        // Demonstrate First Sequence
        this.demonstrate();
    };
    AppComponent.prototype.demonstrate = function () {
        var _this = this;
        var seq = this.sequenceService.getSequence();
        var _loop_1 = function (i) {
            setTimeout(function () {
                _this.squareService.blink(seq[i]);
            }, 800 * i);
        };
        for (var i = 0; i < seq.length; i++) {
            _loop_1(i);
        }
    };
    // User Click Event from Sqaure
    AppComponent.prototype.click = function (ev) {
        var res = this.sequenceService.click(ev);
        if (res === -1) {
            alert('You lose!');
            this.init();
        }
        else if (res === 1) {
            if (confirm('You win! Ready for the next stage?')) {
                this.next();
            }
            else {
                // Restart
                this.init();
            }
        }
    };
    // Next Stage
    AppComponent.prototype.next = function () {
        var random = Math.floor(Math.random() * this.available.length);
        this.sequenceService.add(this.available[random]);
        this.sequenceService.resetClicks();
        this.squareService.resetState();
        this.demonstrate();
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__square_square_service__["a" /* SquareService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__square_square_service__["a" /* SquareService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__sequence_sequence_service__["a" /* SequenceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__sequence_sequence_service__["a" /* SequenceService */]) === "function" && _b || Object])
], AppComponent);

var _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__square_square_component__ = __webpack_require__("../../../../../src/app/square/square.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__square_square_service__ = __webpack_require__("../../../../../src/app/square/square.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sequence_sequence_service__ = __webpack_require__("../../../../../src/app/sequence/sequence.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_3__square_square_component__["a" /* SquareComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__square_square_service__["a" /* SquareService */],
            __WEBPACK_IMPORTED_MODULE_5__sequence_sequence_service__["a" /* SequenceService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/sequence/sequence.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SequenceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SequenceService = (function () {
    function SequenceService() {
        this._sequence = [];
        this._clicks = [];
    }
    SequenceService.prototype.reset = function () {
        while (this._sequence.length > 0) {
            this._sequence.pop();
        }
        while (this._clicks.length > 0) {
            this._clicks.pop();
        }
    };
    SequenceService.prototype.add = function (i) {
        this._sequence.push(i);
    };
    SequenceService.prototype.getSequence = function () {
        return this._sequence;
    };
    SequenceService.prototype.resetClicks = function () {
        while (this._clicks.length > 0) {
            this._clicks.pop();
        }
    };
    SequenceService.prototype.click = function (i) {
        if (this._sequence[this._clicks.length] === i) {
            this._clicks.push(i);
        }
        else {
            return -1;
        }
        if (JSON.stringify(this._sequence) === JSON.stringify(this._clicks)) {
            return 1;
        }
        else {
            return 0;
        }
    };
    return SequenceService;
}());
SequenceService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], SequenceService);

//# sourceMappingURL=sequence.service.js.map

/***/ }),

/***/ "../../../../../src/app/square/square.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"square\" [ngStyle]=\"{backgroundColor: square.color}\" [ngClass]=\"{active: square.active}\" (click)=\"click()\">\n  &nbsp;\n</div>"

/***/ }),

/***/ "../../../../../src/app/square/square.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".square {\n  width: 20vw;\n  height: 20vw;\n  display: inline-block;\n  cursor: pointer;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  margin: 2px; }\n  .square.active {\n    -webkit-filter: brightness(30%);\n            filter: brightness(30%); }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/square/square.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SquareComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__square__ = __webpack_require__("../../../../../src/app/square/square.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__square_service__ = __webpack_require__("../../../../../src/app/square/square.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SquareComponent = (function () {
    function SquareComponent(squareService) {
        this.squareService = squareService;
        this.userClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    SquareComponent.prototype.ngOnInit = function () {
    };
    SquareComponent.prototype.click = function () {
        this.blink();
        this.userClick.emit(this.square.id);
    };
    SquareComponent.prototype.blink = function () {
        this.squareService.blink(this.square.id);
    };
    return SquareComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__square__["a" /* Square */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__square__["a" /* Square */]) === "function" && _a || Object)
], SquareComponent.prototype, "square", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* Output */])(),
    __metadata("design:type", Object)
], SquareComponent.prototype, "userClick", void 0);
SquareComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-square',
        template: __webpack_require__("../../../../../src/app/square/square.component.html"),
        styles: [__webpack_require__("../../../../../src/app/square/square.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__square_service__["a" /* SquareService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__square_service__["a" /* SquareService */]) === "function" && _b || Object])
], SquareComponent);

var _a, _b;
//# sourceMappingURL=square.component.js.map

/***/ }),

/***/ "../../../../../src/app/square/square.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SquareService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__square__ = __webpack_require__("../../../../../src/app/square/square.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SquareService = (function () {
    function SquareService() {
        this._list = [];
    }
    SquareService.prototype.reset = function () {
        while (this._list.length > 0) {
            this._list.pop();
        }
    };
    SquareService.prototype.resetState = function () {
        for (var _i = 0, _a = this._list; _i < _a.length; _i++) {
            var i = _a[_i];
            i.active = false;
        }
    };
    SquareService.prototype.add = function (color) {
        this._list.push(new __WEBPACK_IMPORTED_MODULE_1__square__["a" /* Square */](this._list.length, color));
    };
    SquareService.prototype.getAll = function () {
        return this._list;
    };
    SquareService.prototype.blink = function (id) {
        var _loop_1 = function (i) {
            if (i.id === id) {
                i.active = true;
                setTimeout(function () {
                    i.active = false;
                }, 200);
                return "break";
            }
        };
        for (var _i = 0, _a = this._list; _i < _a.length; _i++) {
            var i = _a[_i];
            var state_1 = _loop_1(i);
            if (state_1 === "break")
                break;
        }
    };
    return SquareService;
}());
SquareService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], SquareService);

//# sourceMappingURL=square.service.js.map

/***/ }),

/***/ "../../../../../src/app/square/square.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Square; });
var Square = (function () {
    function Square(id, color) {
        this._id = id;
        this._color = color;
        this._active = false;
    }
    Object.defineProperty(Square.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "color", {
        get: function () {
            return this._color;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Square.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (val) {
            this._active = val;
        },
        enumerable: true,
        configurable: true
    });
    return Square;
}());

//# sourceMappingURL=square.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_19" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map