import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Square } from './square';
import { SquareService } from './square.service';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {

  @Input() square: Square;
  @Output() userClick = new EventEmitter<Number>();

  constructor(
    private squareService: SquareService
  ) { }

  ngOnInit() {
  }

  public click(): void {
    this.blink();
    this.userClick.emit(this.square.id);
  }

  private blink(): void {
    this.squareService.blink(this.square.id);
  }

}
