import { Injectable } from '@angular/core';

import { Square } from './square';

@Injectable()
export class SquareService {

  private _list: Square[];

  constructor() {
    this._list = [];
  }

  public reset(): void {
    while (this._list.length > 0) {
      this._list.pop();
    }
  }

  public resetState(): void {
    for (const i of this._list) {
      i.active = false;
    }
  }

  public add(color: String): void {
    this._list.push(new Square(this._list.length, color));
  }

  public getAll(): Square[] {
    return this._list;
  }

  public blink(id): void {
    for (const i of this._list) {
      if (i.id === id) {
        i.active = true;
        setTimeout(() => {
          i.active = false;
        }, 200);
        break;
      }
    }
  }

}
