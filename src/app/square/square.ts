export class Square {
  private _id: Number;
  private _color: String;
  private _active: Boolean;

  constructor(
    id: Number,
    color: String
  ) {
    this._id = id;
    this._color = color;
    this._active = false;
  }

  get id(): Number {
    return this._id;
  }

  get color(): String {
    return this._color;
  }

  get active(): Boolean {
    return this._active;
  }

  set active(val: Boolean) {
    this._active = val;
  }

}
