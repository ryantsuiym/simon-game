import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SquareComponent } from './square/square.component';

import { SquareService } from './square/square.service';
import { SequenceService } from './sequence/sequence.service';

@NgModule({
  declarations: [
    AppComponent,
    SquareComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    SquareService,
    SequenceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
