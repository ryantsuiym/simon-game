import { Component } from '@angular/core';

import _ from 'lodash';

import { Square } from './square/square';
import { SquareService } from './square/square.service';
import { SequenceService } from './sequence/sequence.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public squares: Square[];
  private available: Number[];

  constructor(
    private squareService: SquareService,
    private sequenceService: SequenceService
  ) {
    this.init();
  }

  private init(): void {
    // Reset Lists
    this.squareService.reset();
    this.sequenceService.reset();

    // Generate Squares
    this.squareService.add('Blue');
    this.squareService.add('DarkOliveGreen');
    this.squareService.add('CornflowerBlue');
    this.squareService.add('HotPink');
    this.squareService.add('Chocolate');
    this.squareService.add('DarkMagenta');
    this.squareService.add('Gold');
    this.squareService.add('GreenYellow');

    // Get List of Sqaures from SquareService
    this.squares = this.squareService.getAll();

    // Get List of Available Sequence IDs
    this.available = _.map(this.squares, (o) => o.id);

    // Generate Initial Sequence
    let random;
    for (let i = 0; i < 3; i++) {
      random = Math.floor(Math.random() * this.available.length);
      this.sequenceService.add(this.available[random]);
    }

    // Demonstrate First Sequence
    this.demonstrate();
  }

  private demonstrate(): void {
    const seq = this.sequenceService.getSequence();
    for (let i = 0; i < seq.length; i++) {
      setTimeout(() => {
        this.squareService.blink(seq[i]);
      }, 800 * i);
    }
  }

  // User Click Event from Sqaure
  public click(ev: Number): void {
    const res = this.sequenceService.click(ev);
    if (res === -1) {
      alert('You lose!');
      this.init();
    } else if (res === 1) {
      if (confirm('You win! Ready for the next stage?')) {
        this.next();
      } else {
        // Restart
        this.init();
      }
    }
  }

  // Next Stage
  private next(): void {
    const random = Math.floor(Math.random() * this.available.length);
    this.sequenceService.add(this.available[random]);
    this.sequenceService.resetClicks();
    this.squareService.resetState();
    this.demonstrate();
  }
}
