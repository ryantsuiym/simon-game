import { Injectable } from '@angular/core';

@Injectable()
export class SequenceService {

  private _sequence: Number[];
  private _clicks: Number[];

  constructor() {
    this._sequence = [];
    this._clicks = [];
  }

  public reset(): void {
    while (this._sequence.length > 0) {
      this._sequence.pop();
    }

    while (this._clicks.length > 0) {
      this._clicks.pop();
    }
  }

  public resetClicks(): void {
    while (this._clicks.length > 0) {
      this._clicks.pop();
    }
  }

  public add(i: Number): void {
    this._sequence.push(i);
  }

  public getSequence(): Number[] {
    return this._sequence;
  }

  public click(i: Number): Number {
    if (this._sequence[this._clicks.length] === i) {
      this._clicks.push(i);
    } else {
      return -1;
    }

    if (JSON.stringify(this._sequence) === JSON.stringify(this._clicks)) {
      return 1;
    } else {
      return 0;
    }
  }

}
